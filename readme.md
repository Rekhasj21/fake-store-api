
<h1 align="center">Rest Countries API</h1>

## Table of Contents

- [Overview](#overview)
- [Project Link](#project-link)
- [Built With](#built-with)
- [How to use](#how-to-use)
- [Contact](#contact)

## Overview

<a href="" rel="image text"><img src="public/fake-store-api.png" alt="" /></a>

- Display list of Products
- Add to cart
- Filters products based on size
- Increment and Decrement the quantity of the product.
- Checkout displays the total price of the products in the cart.

## Project Link

<a href="https://rekha-sj-fake-store-api.netlify.app/" >Click To Check Project Link</a>

### Built With

- HTML
- CSS
- Bootstrap
- React JS

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone https://gitlab.com/Rekhasj21/fake-store-api

# Install dependencies
$ npm install

# Run the app
$ npm start
```

## Contact

- Website (<https://rekha-sj-fake-store-api.netlify.app/>)
- GitHub (<https://gitlab.com/Rekhasj21/fake-store-api>)