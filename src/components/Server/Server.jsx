import axios from 'axios'

export default function getProductsData() {
    const productsData = axios.get('https://fakestoreapi.com/products')
        .then(response => response.data)
        .catch(error => console.log(error))
    return productsData
}
