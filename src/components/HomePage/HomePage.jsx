import React, { useState, useReducer } from 'react'
import DisplayProducts from '../DisplayProducts/DisplayProducts'
import products from '../../../shopping-cart/data.json'
import Filter from '../Filter/Filter'
import Cart from '../Cart/Cart';

export const FilteringContext = React.createContext()
const initialState = {
    cartItems: [],
    totalPrice: 0,
};

function reducer(state, action) {
    switch (action.type) {
        case 'ADD_TO_CART':
            const existingItem = state.cartItems.find(item => item.id === action.payload.id);
            if (existingItem) {
                return {
                    ...state,
                    cartItems: state.cartItems.map(item => {
                        if (item.id === action.payload.id) {
                            return {
                                ...item,
                                quantity: item.quantity + 1,
                            };
                        }
                        return item;
                    }),
                    totalPrice: state.totalPrice + action.payload.price,
                };
            }
            return {
                ...state,
                cartItems: [
                    ...state.cartItems,
                    {
                        ...action.payload,
                        quantity: 1,
                    },
                ],
                totalPrice: state.totalPrice + action.payload.price,
            };
        case 'REMOVE_FROM_CART':
            const itemToRemove = state.cartItems.find(item => item.id === action.payload.id);
            if (itemToRemove.quantity === 1) {
                return {
                    ...state,
                    cartItems: state.cartItems.filter(item => item.id !== action.payload.id),
                    totalPrice: state.totalPrice - itemToRemove.price,
                };
            }
            return {
                ...state,
                cartItems: state.cartItems.map(item => {
                    if (item.id === action.payload.id) {
                        return {
                            ...item,
                            quantity: item.quantity - 1,
                        };
                    }
                    return item;
                }),
                totalPrice: state.totalPrice - itemToRemove.price,
            };
        case 'REMOVE_ITEM_FROM_CART':
            const removeItem = state.cartItems.find(item => item.id === action.payload.id);
            return {
                ...state,
                cartItems: state.cartItems.filter(item => item.id !== action.payload.id),
                totalPrice: state.totalPrice - (removeItem.quantity * removeItem.price),
            }
        default:
            return state;
    }
}

function HomePage() {
    const [productsData, setProductsData] = useState(products.products);
    const [cartState, cartDispatch] = useReducer(reducer, initialState);
    const sizes = [...new Set(products.products.flatMap(p => p.availableSizes))]
    const [isOpen, setIsOpen] = useState(false);
    let display = isOpen ? 'show' : ''

    const handleFilterClick = (activeSizes) => {
        const filteredProducts = products.products.filter(product => {
            if (activeSizes.length !== 0) {
                return activeSizes.some(activeSize => {
                    return product.availableSizes.includes(activeSize);
                });
            }
            return productsData
        });
        setProductsData(filteredProducts);
    }

    const onClickAddToCart = (cartItemId) => {
        setIsOpen(true)
        const cartProduct = productsData.filter(product => product.id === cartItemId)
        cartDispatch({
            type: 'ADD_TO_CART',
            payload: {
                id: cartProduct[0].id,
                name: cartProduct[0].title,
                price: cartProduct[0].price,
                size: cartProduct[0].availableSizes[0],
                style: cartProduct[0].style,
                currencyFormat: cartProduct[0].currencyFormat,
                quantity: cartState.quantity,
                imageUrl: `/${cartItemId}_2.jpg`,
            },
        });
    }
    const onClickIncrementQuantity = (product) => {
        cartDispatch({
            type: 'ADD_TO_CART',
            payload: {
                id: product.id,
                title: product.title,
                price: product.price,
                imageUrl: `/${product.id}_2.jpg`,
            },
        });
    };
    const onClickDecrementQuantity = (product) => {
        cartDispatch({
            type: 'REMOVE_FROM_CART',
            payload: {
                id: product.id,
                price: product.price,
            },
        });
    };
    const onClickCloseItem = (product) => {
        cartDispatch({
            type: 'REMOVE_ITEM_FROM_CART',
            payload: {
                id: product.id,
            },
        });
    }
    const handleCheckout = () => {
        alert(`Checkout - Subtotal: $ ${cartState.totalPrice.toFixed(2)}`)
    }
    const handleClose = () => {
        setIsOpen(false)
    }
    return (
        <>
            <nav className="navbar  d-flex justify-content-between align-items-end ">
                <img
                    className="bg-dark text-end"
                    src="/githubicon.png"
                    alt="github icon"
                    width={70}
                    height={70}
                />
                <div className='d-flex flex-column position-relative'>
                    <a className='btn'
                        data-bs-toggle="offcanvas"
                        href="#offcanvasRight" role="button" aria-controls="offcanvasRight"
                    >
                        <img
                            className="bg-dark text-end p-3"
                            src="/bag-icon.png"
                            alt="cart icon"
                            width={50}
                            height={50}
                        />
                        <button className='btn btn-warning rounded-circle text-dark 
                    position-absolute top-50 start-50 translate-end' style={{
                                fontSize: "8px",
                                width: "1.5rem",
                                height: "1.5rem",
                                padding: "0.5rem",
                            }}>{cartState.cartItems.length}</button>
                    </a>
                </div>
            </nav>
            <div className="d-flex">
                <div className="m-5"></div>
                <FilteringContext.Provider value={sizes}>
                    <Filter onFilterClick={handleFilterClick} />
                </FilteringContext.Provider>
                <div className="w-75">
                    <p>{productsData.length} Product(s) found</p>
                    <div className="d-flex flex-wrap">
                        {productsData.map((product) => (
                            <DisplayProducts
                                key={product.id}
                                product={product}
                                imageUrl={`/${product.id}_1.jpg`}
                                onClickAddToCart={onClickAddToCart}
                            />
                        ))}
                    </div>
                </div>
            </div>
            <div className={`offcanvas offcanvas-end  text-bg-dark ${display}`}
                data-bs-scroll="true"
                data-bs-backdrop="false" tabIndex={-1} id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
                <div className="offcanvas-header">
                    <button type="button" className="btn-close bg-white" data-bs-dismiss="offcanvas" aria-label="Close" onClick={handleClose} ></button>
                </div>
                <div className="offcanvas-body text-center">
                    <div className='d-flex position-relative justify-content-center align-items-center m-2'>
                        <img
                            className="bg-dark text-end p-3"
                            src="/bag-icon.png"
                            alt="cart icon"
                            width={60}
                            height={60}
                            data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight"
                        />
                        <button className='btn btn-warning position-absolute top-50 rounded-circle text-dark' style={{
                            fontSize: "8px",
                            width: "1.5rem",
                            height: "1.5rem",
                            padding: "0.5rem",
                        }}>{cartState.cartItems.length}</button>
                        <h1 className='fs-4 '>Cart</h1>
                    </div>
                    <div className='d-flex flex-column bg-dark text-white'>
                        {cartState.cartItems.length > 0 ? (cartState.cartItems.map(cartItem => (
                            <Cart key={cartItem.id} cartItem={cartItem} onClickCloseItem={onClickCloseItem}
                                onClickIncrementQuantity={onClickIncrementQuantity}
                                onClickDecrementQuantity={onClickDecrementQuantity} />))) : (
                            <>
                                <p className='fs-6'>Add some products in the cart</p>
                                <span>:)</span>
                            </>
                        )}
                    </div>
                </div>
                <div className="offcanvas-footer">
                    <div className="offcanvas-footer card shadow p-3 mb-5 bg-body-tertiary rounded bg-black text-white">
                        <div className='d-flex justify-content-between'>
                            <h5 className="card-title text-white text-opacity-50 fs-6">SUBTOTAL</h5>
                            <div className="d-flex flex-column justify-content-end align-items-end">
                                <span className='text-warning fs-4'>$ {cartState.totalPrice.toFixed(2)}</span>
                                <span className='text-white fs-6 text-opacity-50'>OR UP TO 9 x $ </span>
                            </div>
                        </div>
                        <button className='btn btn-dark text-white' onClick={handleCheckout}>CHECKOUT</button>
                    </div>
                </div>
            </div>
        </>
    )
}
export default HomePage