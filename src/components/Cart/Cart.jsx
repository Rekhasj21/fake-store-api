import React from 'react'

function Cart({ cartItem, onClickDecrementQuantity, onClickIncrementQuantity, onClickCloseItem }) {
    const { id, name, price, quantity, imageUrl, style, size, currencyFormat } = cartItem

    const handleDecrementProduct = () => {
        onClickDecrementQuantity(cartItem)
    }
    const handleIncrementProduct = () => {
        onClickIncrementQuantity(cartItem)
    }
    const handleCloseItem = () => {
        onClickCloseItem(cartItem)
    }
    return (
        <>
            <div className="d-flex justify-content-center shadow-sm p-3 mb-5 bg-body-tertiary rounded" style={{ width: "20rem" }} >
                <img src={imageUrl} className="card-img-top" style={{ width: "4rem" }} alt="..." />
                <div className="card-body d-flex justify-content-between text-white">
                    <div className='d-flex flex-column align-items-start m-1'>
                        <h5 className="card-title fs-6 ">{name}</h5>
                        <span className='text-white text-opacity-50'>{size} | {style.split(' ')[0]}</span>
                        <p className='text-white text-opacity-50'>Quantity:{quantity}</p>
                    </div>
                    <div className='d-flex flex-column align-items-center text-light'>
                        <button type="button" className="btn text-light opacity-50" data-bs-dismiss="modal" aria-label="Close" onClick={handleCloseItem}>x</button>                         <span className='text-warning'>{currencyFormat}{price}</span>
                        <div className='d-flex'>
                            <button className='btn btn-dark btn-sm me-1' onClick={handleDecrementProduct}>-</button>
                            <button className='btn btn-dark btn-sm me-1' onClick={handleIncrementProduct}>+</button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Cart