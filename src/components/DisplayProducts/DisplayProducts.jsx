import React from 'react'

function DisplayProducts({ product, imageUrl, onClickAddToCart }) {
    const [priceBeforeDecimal, priceAfterDecimal] = product.price.toString().split('.')
    const installment = (product.price / product.installments).toFixed(2)

    const handleAddToCart = () => {
        onClickAddToCart(product.id)
    }
    return (
        <>
            <div className="card m-3 shadow  border-0 position: relative" style={{ width: "18rem" }} >
                <img src={imageUrl} className="card-img-top bg-secondary" alt="..." />
                <div className="card-body d-flex flex-column justify-content-center align-items-center">
                    <h5 className="card-title opacity-75 text-center h-25">{product.title}</h5>
                    <hr className='border border-2 border-warning w-25 m-0' />
                    <p className='fs-4'>
                        <span className='fs-6'>{product.currencyFormat}</span>
                        {priceBeforeDecimal}
                        {priceAfterDecimal && <span className='fs-6'>.{priceAfterDecimal}</span>}
                    </p>
                    <p className='opacity-50'>{`or ${product.installments} x${product.currencyFormat}${installment}`}</p>
                    <button className="btn btn-dark " data-bs-open="offcanvas"
                        data-bs-target="#offcanvasRight" aria-controls="offcanvasRight"
                        onClick={handleAddToCart} style={{ paddingLeft: "5rem", paddingRight: "5rem" }}>
                        Add to Cart
                    </button>
                </div>
                {product.isFreeShipping && <button type="button" className='btn btn-dark btn-sm position-absolute end-0 top-0'>Free Shipping</button>}
            </div>
        </>
    )
}

export default DisplayProducts