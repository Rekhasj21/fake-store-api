import React, { useContext, useEffect, useState } from 'react'
import { FilteringContext } from '../HomePage/HomePage'

function Filter({ onFilterClick }) {
    const [activeSizes, setActiveSizes] = useState([])
    const sizes = useContext(FilteringContext)

    const handleClick = (size) => {
        let newSizes
        if (activeSizes.includes(size)) {
            newSizes = activeSizes.filter((s) => s !== size)
        } else {
            newSizes = [...activeSizes, size]
        }
        setActiveSizes(newSizes)
    }
    useEffect(() => {
        onFilterClick(activeSizes)
    }, [activeSizes])
    return (
        <div className="p-2 w-25">
            <h1 className='fs-4'>Sizes: </h1>
            {sizes.map((size) => (
                <button
                    className={`btn btn-secondary opacity-75 text-black rounded-circle m-2 text-center ${activeSizes.includes(size) ? "bg-dark text-white" : ""
                        }`}
                    key={size}
                    onClick={() => handleClick(size)}
                    style={{
                        fontSize: "10px", width: "3rem", height: "3rem", padding: "0.5rem",
                    }}>
                    {size}
                </button>
            ))}
            <p>Leave a star on Github if this repository was useful :)</p>
            <a href='' className='fs-5'>Star</a>
        </div>
    )
}

export default Filter
